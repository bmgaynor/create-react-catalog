#!/usr/bin/env node
const scaffold = require('scaffolded')
const path = require('path')

scaffold([
  {
    name: 'moduleName',
    message: 'Whats the name of your module?',
    default: path.basename(process.cwd()).trim()
  },
  {
    name: 'author',
    message: 'Who is the author of the module? {Name} <{email}> (Website)'
  }
], ['catalog/**/*', 'templates/**/*', 'lib/**/*', 'rollup.config.js', '.gitlab-ci.yml', 'README.md'])