export default {
  src: './lib',
  base: '/create-react-catalog/',
  dest: 'public',
  public: '/assets' // required to override this since default is the dest 
}