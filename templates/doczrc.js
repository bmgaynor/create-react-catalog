export default {
  src: './lib',
  base: '/<%= properties.moduleName %>/',
  dest: 'public',
  public: '/assets' // required to override this since default is the dest 
}